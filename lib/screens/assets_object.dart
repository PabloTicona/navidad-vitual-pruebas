import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart';
import 'package:arcore_flutter_plugin_example/models/object3d.dart';
import 'package:arcore_flutter_plugin_example/screens/augmented_faces.dart';
import 'package:arcore_flutter_plugin_example/widgets/item_object.dart';
import 'package:flutter/material.dart';
import 'dart:typed_data';

import 'package:vector_math/vector_math_64.dart' as vector;

import 'package:flutter/services.dart';

class AssetsObject extends StatefulWidget {
  @override
  _AssetsObjectState createState() => _AssetsObjectState();
}

class _AssetsObjectState extends State<AssetsObject> {
  ArCoreController arCoreController;

  List<Object3d> listObjects;
  Object3d userSelect3d;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getData();
  }

  getData() {
    List<Object3d> data = [];
    /*   data.add(new Object3d(
        image: "assets/TocoToucan.gif", name: "Tucan", file: "toucan.sfb"));
    data.add(new Object3d(
        image: "assets/AndroidRobot.gif", name: "Android", file: "andy.sfb"));

    */

    data.add(
      new Object3d(
          image: "assets/img/arbol.jpg",
          name: "Arbol",
          file: "arbol.sfb",
          tipo: "",
          rotation: vector.Vector4(0, 30, 25, 0),
          position: null,
          scale: null),
    );
    data.add(new Object3d(
        image: "assets/img/caja.jpg",
        name: "Caja",
        file: "caja.sfb",
        tipo: "",
        rotation: vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: null));
    data.add(new Object3d(
        image: "assets/img/estrella.jpg",
        name: "Estrella0",
        file: "estrella.sfb",
        tipo: "",
        rotation: vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: null));
    data.add(new Object3d(
        image: "assets/img/pinguino.jpg",
        name: "Pinguino",
        file: "pinguino.sfb",
        tipo: "",
        rotation: vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: null));
    data.add(new Object3d(
        image: "assets/img/nieve.jpg",
        name: "Nieve",
        file: "nieve.sfb",
        tipo: "",
        rotation: vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: null));
    data.add(new Object3d(
        image: "assets/img/nieve.jpg",
        name: "SantaModel",
        file: "SantaModel.glb",
        tipo: "",
        rotation: vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: vector.Vector3(0.3, 0.3, 0.3)));

    data.add(new Object3d(
        image: "assets/img/nieve.jpg",
        name: "Sabrewulf",
        file: "Sabrewulf.glb",
        tipo: "",
        rotation: vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: vector.Vector3(0.3, 0.3, 0.3)));
    /*data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "gato",
        file: "gato.glb",
        tipo: "",
        rotation:null,// vector.Vector4(0, 30, 25, 0),
        position: null,
        scale: null
        ));*/

/**
 * Default
 * */
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-8",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -8, -5),
        scale: vector.Vector3(0.3, 0.3, 0.3)));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-7",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -7, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-6",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -6, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -5, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-4",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -4, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-3",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -3, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-2",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -2, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-1",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, -1, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "0",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, 0, -5),
        scale: null));

    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "1",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, 1, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "2",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, 2, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "3",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(8, 3, -5),
        scale: null));
//-----------------------------------------------------
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "4",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(7, 4, -5),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "4.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(5.5, 4.5, -5),
        scale: null));
//-----------------------------------------------------------------------
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "5.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(5, 5.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "5.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(4, 5.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "5.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(3, 5.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "5.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(2, 5.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "5.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(1, 5.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(0, 5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "4.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-1, 4.5, -6.0),
        scale: null));
    //------------------------------------------------------------
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "4",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, 4, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "3",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, 3, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "2",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, 2, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "1",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, 1, -6.0),
        scale: null));

    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "0",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, 0, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-1.0",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -1.0, -6.0),
        scale: null));

    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-2.0",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -2.0, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-3.0",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -3.0, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-4.0",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -4.0, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-5.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -5.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-6.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -6.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-7.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -7.5, -6.0),
        scale: null));
    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "-8.5",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(-2, -8.5, -6.0),
        scale: null));

    /*data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "bola2",
        file: "bola.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(0, 2, -1.5),
        scale: null));*/

    /*data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "AnimatedMorphSphere",
        file: "AnimatedMorphSphere.glb",
        tipo: "default",
        rotation: null,
        position: vector.Vector3(0, -0.9, -2.0),
        scale: null));*/
/**
 * IMG
 * */
    /*data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "bola2",
        file: "assets/img/fondo2.png",
        tipo: "img",
        rotation: null,
        position: vector.Vector3(0,-2,-9.5,),
        scale: null));*/

    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "bola2",
        file: "assets/imagenes/lazo.png",
        tipo: "img",
        rotation: null,
        position: vector.Vector3(-2, 4, -5.5),
        scale: null));

    data.add(new Object3d(
        image: "assets/ArcticFox.gif",
        name: "bola2",
        file: "assets/imagenes/lazo.png",
        tipo: "img",
        rotation: null,
        position: vector.Vector3(7, 4, -4.5),
        scale: null));

        


    setState(() {
      listObjects = data;
    });
  }

  selectItemUser(Object3d object3d) {
    //  Navigator.of(context).pop();
    setState(() {
      userSelect3d = object3d;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black.withOpacity(0.1),
          elevation: 0.0,
          actions: [
            IconButton(
              icon: const Icon(Icons.face_retouching_natural),
              tooltip: 'Retoques',
              onPressed: retoques,
            ),
            IconButton(
              icon: const Icon(Icons.add_a_photo),
              tooltip: 'Guardar en la galeria',
              onPressed: saveGalery,
            ),
            PopupMenuButton(
                itemBuilder: (context) => [
                      PopupMenuItem(
                        child: Row(
                          children: [
                            Icon(Icons.share),
                            Text("Síguenos en Facebook")
                          ],
                        ),
                        value: 1,
                      ),
                      PopupMenuItem(
                        child: Row(
                          children: [
                            Icon(
                              Icons.share,
                              color: Colors.black,
                            ),
                            Text("Comparte la aplicación")
                          ],
                        ),
                        value: 2,
                      )
                    ])
          ],
          title: (userSelect3d != null)
              ? Row(
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      child: Image.asset(userSelect3d.image, fit: BoxFit.cover),
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                      child: Text(userSelect3d.name),
                    )
                  ],
                )
              : Text(""),
        ),
        extendBodyBehindAppBar: true,
        body: Container(
            width: double.infinity,
            height: double.infinity,
            child: Padding(
              padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              child: Stack(
                children: <Widget>[
                  ArCoreView(
                    onArCoreViewCreated: _onArCoreViewCreated,
                    enableTapRecognizer: true,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: double.infinity,
                      height: 95,
                      decoration:
                          BoxDecoration(color: Colors.black.withOpacity(0.1)),
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          padding:
                              const EdgeInsets.fromLTRB(2.0, 2.0, 0.0, 3.0),
                          itemCount: listObjects.length,
                          itemBuilder: (BuildContext context, int index) {
                            if (listObjects[index].tipo != "default" &&
                                listObjects[index].tipo != "img") {
                              return Container(
                                child: ItemObject(
                                  object3d: listObjects[index],
                                  active: (userSelect3d != null)
                                      ? listObjects[index].file ==
                                          userSelect3d.file
                                      : false,
                                  onPressed: selectItemUser,
                                ),
                              );
                            } else {
                              return Container();
                            }
                          }),
                    ),
                  ),
                ],
              ),
            )),
        /*  floatingActionButton: FloatingActionButton(
          onPressed: () {
            _showListObject(listObjects);
          },
          tooltip: 'Increment',
          child: const Icon(
            Icons.add,
          ),
          elevation: 0.0,
          backgroundColor: Colors.black.withOpacity(0.1),
        ),*/
      ),
    );
  }

  void _onArCoreViewCreated(ArCoreController controller) {
    arCoreController = controller;
    arCoreController.onNodeTap = (name) => onTapHandler(name);
    arCoreController.onPlaneTap = _handleOnPlaneTap;

    if (listObjects != null) {
      for (int i = 0; i < listObjects.length; i++) {
        if (listObjects[i].tipo == "default") {
          _addObject3DDefault(
              controller: controller, userSelect3d: listObjects[i]);
        }
        if (listObjects[i].tipo == "img") {
          _addImage(controller: controller, userSelect3d: listObjects[i]);
        }
        /*if (listObjects[i].tipo == "img") {
          _addImg(controller: controller, userSelect3d: listObjects[i]);
        }*/
      }
    }
  }

  void _addObject3D(ArCoreHitTestResult plane) {
    if (userSelect3d != null) {
      //"https://github.com/KhronosGroup/glTF-Sample-Models/raw/master/2.0/Duck/glTF/Duck.gltf"

      var position_ = (userSelect3d.position != null)
          ? plane.pose.translation + userSelect3d.position
          : plane.pose.translation;
      var rotation_ = (userSelect3d.rotation != null)
          ? plane.pose.rotation + userSelect3d.rotation
          : plane.pose.rotation;

      var extGLB = userSelect3d.file.toString().contains(".glb");
      var extSFB = userSelect3d.file.toString().contains(".sfb");

      var toucanoNode = ArCoreReferenceNode();

      if (extGLB) {
        toucanoNode = ArCoreReferenceNode(
            name: userSelect3d.name,
            // scale: vector.Vector3(0.3, 0.3, 0.3),
            scale: userSelect3d.scale,
            objectUrl: userSelect3d.file,

            //position: position_,
            //rotation: rotation_,

            //scale: vector.Vector3(0.5, 0.5, 0.5),
            //object3DFileName: userSelect3d.file,
            position: plane.pose.translation,
            //rotation: plane.pose.rotation + vector.Vector4(0, 30, 25, 0),
            rotation: plane.pose.rotation);
      }
      if (extSFB) {
        toucanoNode = ArCoreReferenceNode(
          name: userSelect3d.name,
          //  scale: vector.Vector3(0.3, 0.3, 0.3),
          scale: userSelect3d.scale,
          object3DFileName: userSelect3d.file,
          position: position_,
          rotation: rotation_,
        );
      }

      arCoreController.addArCoreNodeWithAnchor(toucanoNode);
    }
  }

  void _addObject3DDefault(
      {ArCoreController controller, Object3d userSelect3d}) {
    if (userSelect3d != null) {
      var extGLB = userSelect3d.file.toString().contains(".glb");
      var extSFB = userSelect3d.file.toString().contains(".sfb");

      var toucanoNode = ArCoreReferenceNode();

      if (extGLB) {
        toucanoNode = ArCoreReferenceNode(
            name: userSelect3d.name,
            // scale: vector.Vector3(0.3, 0.3, 0.3),
            scale: userSelect3d.scale,
            // object3DFileName: userSelect3d.file,
            objectUrl: userSelect3d.file,
            position: userSelect3d.position);
      }

      if (extSFB) {
        toucanoNode = ArCoreReferenceNode(
            name: userSelect3d.name,
            scale: userSelect3d.scale,
            //scale: vector.Vector3(-1.8, -1.8, -1.8),
            object3DFileName: userSelect3d.file,
            // objectUrl: userSelect3d.file,
            position: userSelect3d.position);
      }

      controller.addArCoreNode(toucanoNode);
    }
  }

  void _addImg({ArCoreController controller, Object3d userSelect3d}) async {
    final ByteData textureBytes = await rootBundle.load(userSelect3d.file);

    final material = ArCoreMaterial(
        color: Color.fromARGB(120, 66, 134, 244),
        textureBytes: textureBytes.buffer.asUint8List());
    final sphere = ArCoreSphere(
      materials: [material],
      radius: 0.1,
    );
    final node = ArCoreNode(
      shape: sphere,
      position: vector.Vector3(1, 0, -0.5),
    );
    controller.addArCoreNode(node);
  }

  Future _addImage({ArCoreController controller, Object3d userSelect3d}) async {
    final bytes =
        (await rootBundle.load(userSelect3d.file)).buffer.asUint8List();

    final earth = ArCoreNode(
      image: ArCoreImage(bytes: bytes, width: 1200, height: 1200),
      position:  userSelect3d.position, //vector.Vector3(-2, 4, -5.5),
      rotation: vector.Vector4(0.0, 0.0, 0.0, 0.0),
    );

    controller.addArCoreNode(earth);
  }

  void _handleOnPlaneTap(List<ArCoreHitTestResult> hits) {
    final hit = hits.first;
    _addObject3D(hit);
  }

  void onTapHandler(String name) {
    showDialog<void>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        content: Row(
          children: <Widget>[
            Text('¿Eliminar $name?'),
            IconButton(
                icon: Icon(
                  Icons.delete,
                ),
                onPressed: () {
                  try {
                    arCoreController.removeNode(nodeName: name);
                  } catch (e) {}

                  Navigator.pop(context);
                })
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    arCoreController.dispose();
    super.dispose();
  }

  void saveGalery() {
    //  arCoreController.
  }

  void retoques() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => AugmentedFacesScreen()));
  }
}
