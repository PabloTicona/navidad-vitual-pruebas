import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart' as vector;

class Object3d {
  String image;
  String name;
  String file;
  String tipo;
  vector.Vector4 rotation;
  vector.Vector3 position;
  vector.Vector3 scale;

  Object3d(
      {@required this.image,
      @required this.name,
      @required this.file,
      @required this.tipo,
      @required this.rotation,
      @required this.position,
      @required this.scale
      });
}
