import 'app.dart';
import 'package:flutter/material.dart';
import 'package:arcore_flutter_plugin/arcore_flutter_plugin.dart'
    show ArCoreController;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  print(await ArCoreController.checkArCoreAvailability());
  print(await ArCoreController.checkIsArCoreInstalled());
  runApp(App());
}
