import 'package:arcore_flutter_plugin_example/models/object3d.dart';
import 'package:flutter/material.dart';

typedef void PressCallback(Object3d object3d);

class ItemObject extends StatelessWidget {
  final Object3d object3d;
  bool active;
  PressCallback onPressed;

  ItemObject({this.object3d,  this.active, this.onPressed});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new InkWell(
      child: Container(
        padding: EdgeInsets.fromLTRB(6.0, 0.0, 6.0, 0.0),
        //height: 10,
        // color: Colors.amber[colorCodes[index]],
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(0.0),

              child: Icon(Icons.keyboard_arrow_up, color:active? Colors.white:Colors.black.withOpacity(0.1),size: 22.0,),
            ),


            Container(
              width: 50,
              height: 50,
              color: Colors.white54,
              child: Image.asset(object3d.image),
            ),
            Padding(
              padding: EdgeInsets.all(0.0),
              child: Text(
                object3d.name,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 13.0,
                    fontWeight: active ? FontWeight.bold : FontWeight.normal, ),
              ),
            )
          ],
        ),
      ),
      onTap: () => onPressed(object3d),
    );
  }
}
